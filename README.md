# HTML5

Diese Präsentation habe ich an den OC-Days 2013 gehalten.

## Setup

Basis der Präsentation ist [reveal.js](https://github.com/hakimel/reveal.js).

Hier ist die Anleitung:

1. Install [Node.js](http://nodejs.org/)

2. Install [Grunt](http://gruntjs.com/getting-started#installing-the-cli)

4. Clone the presentation  
```
$ git clone https://phjardas@bitbucket.org/phjardas/html5.git
$ cd html5
```

5. Install dependencies  
```
$ npm install
```

6. Serve the presentation and monitor source files for changes  
```
$ grunt serve
```

7. Open <http://localhost:8000> to view your presentation
