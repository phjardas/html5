Philipp Jardas
--------------

Senior Consultant · Bad Homburg

### Projekte

Abu Dhabi · Auswärtiges Amt · Bundespräsident

Land Berlin · TNT · Kombiverkehr

### Technologien

Java · Spring · EAI · HTML5

Java-/CoffeeScript · AngularJS · node.js · MongoDB

### Und sonst so

TEN SING · Musik · Fußball
