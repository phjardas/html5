Technologien
------------

* ![Offline & Storage](img/tech-classes-32/offline.png) Offline & Storage
* ![Connectivity / Realtime](img/tech-classes-32/connectivity.png) Connectivity / Realtime
* ![Device Access](img/tech-classes-32/device.png) Device Access
* ![Semantics](img/tech-classes-32/semantics.png) Semantics
* ![Multimedia](img/tech-classes-32/multimedia.png) Multimedia
* ![Graphics, 3D & Effects](img/tech-classes-32/3d.png) Graphics, 3D & Effects
* ![CSS3 / Styling](img/tech-classes-32/css3.png) CSS3 / Styling
* ![Performance & Integration](img/tech-classes-32/performance.png) Performance & Integration
