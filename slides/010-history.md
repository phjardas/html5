Geschichte von HTML
-------------------

* 1991: **HTML**
* 1994: **HTML 2**
* 1996: **CSS** + **JavaScript**
* 1997: **HTML 4**
* 1998: **CSS 2**
* 2000: **XHTML 1**
* 2002: **<del>Table Design</del>** &lt;div>!
* 2005: **AJAX**
* 2009: **HTML 5**
